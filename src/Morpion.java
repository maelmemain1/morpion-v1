/*
 * Morpion jcm alea
 * @author marloxdev
 */

 import java.util.ArrayList;
 import java.util.*;
 import java.io.*;
 
 public class Morpion{

    private static String[] jeu = new String[9];
    private static ArrayList<Integer> coups = new ArrayList<Integer>();
    private static int caseJ = -1;
    private static int tour = 0;
    private static int version = 1; //Numéro de la version
    private static String vBot = "coup gagnant v1O";
    private static String etat = "En developpement";
    private static int gagne = 0; // 1 si partie gagné
    private static String j2;

    public static void main(String[] args) {
        intro();

        String j = SimpleInput.getString("\033[0;1mEntrez votre nom : ");
        j2 = j;

        jouer(jeu);
        
    }

    public static void jouer (String[] jeu){
        coups.clear();
        gagne = 0;
        tour = 0;

        remplirTab(jeu);

        boolean test = false;

        System.out.println();
        
        String jActuel;
        String j1 = "Bot";
        /*String j2 = "Mael";*/
        jActuel = j1;
        int random = (int) (Math.random() * 2);
        if (random % 2 == 0){
        jActuel = j2;
        }
        afficher(jeu);

        while ((peutJouer(jeu) == true) && (gagne == 0)){

            attendre(1000);

            jActuel = changeJoueur(jActuel, j1, j2);

            caseJ = -1;
            
            //Lancement du jeu
            if (jActuel == j2){

                while (estPossible(jeu, caseJ) == false){
                caseJ = SimpleInput.getInt("Entrez le numéro de la case ou vous souhaitez jouer : ");
                }
                coups.add(caseJ);

                remplace(jeu, caseJ, jActuel,  j1, j2);

            } else {

                caseJ = botJoue(jeu);
                while (estInclus(caseJ, coups) == true){
                    caseJ = botJoue(jeu);
                }

                coups.add(caseJ);
                remplace(jeu, caseJ, jActuel, j1, j2);
            }

            aJoue(jActuel, j1, j2, caseJ);

            afficher(jeu);

            tour++;

        }
        if (gagne == 1){
            System.out.println("\033[1;36m" + jActuel + " a gagné !\033[0;1m");
        } else {
            System.out.println("\033[1;36mMatch nul !\033[0;1m");
        }

        rejouer();
     }

     

    public static void afficher (String[] jeu){

        System.out.println();
        System.out.println("-------------");
        System.out.println("\033[0;1m| \u001B[33m" + jeu[0] + "\u001B[37m | \u001B[33m" + jeu[1] + "\u001B[37m | \u001B[33m" + jeu[2] + "\u001B[37m |");
        System.out.println("-------------");
        System.out.println("\033[0;1m| \u001B[33m" + jeu[3] + "\u001B[37m | \u001B[33m" + jeu[4] + "\u001B[37m | \u001B[33m" + jeu[5] + "\u001B[37m |");
        System.out.println("-------------");
        System.out.println("\033[0;1m| \u001B[33m" + jeu[6] + "\u001B[37m | \u001B[33m" + jeu[7] + "\u001B[37m | \u001B[33m" + jeu[8] + "\u001B[37m |");
        System.out.println("-------------");
        System.out.println();
     }

    public static void remplirTab (String[] jeu){
        jeu[0] = "1";
        jeu[1] = "2";
        jeu[2] = "3";
        jeu[3] = "4";
        jeu[4] = "5";
        jeu[5] = "6";
        jeu[6] = "7";
        jeu[7] = "8";
        jeu[8] = "9";
     }

     /**
      * Permet de déterminer si il est possible de jouer sur le plateau
      * @param jeu le plateau du morpion
      * @return true si on il est encore possible de jouer
      */
    public static boolean peutJouer (String[] jeu){
        boolean ret = true;
        boolean test = false;
        int possible = 0;
        int i = 0;
        
        while ((i < 8) && (test == false)){

            if (jeu[i] != "\033[1;31mo" && jeu[i] != "\033[1;32mx"){
                possible++;

            }

            if (possible > 0){
                test = true;
            }
            
            i++;
        }

        if (possible == 0){
            ret = false;
        }

        if (jeuGagnant(jeu) == true){
            ret = false;
        }

        return ret;
    }

    /**
     * Permet de déterminer si on peut jouer dans une case demandée
     * @param jeu le plateau du morpion
     * @param caseJ la case demandée
     * @return true si le coup est possible
     */
    public static boolean estPossible (String[] jeu, int caseJ){
        boolean ret = true;

        if (caseJ > 9 || caseJ < 1){
            ret = false;

        } else {

            if ((jeu[(caseJ-1)] == "\033[1;31mo") || (jeu[(caseJ-1)] == "\033[1;32mx")){
                ret = false;
            }
        }

        return ret;
     }

    public static void remplace (String[] jeu, int caseJ, String jActuel, String j1, String j2){
        if (caseJ > 9 || caseJ < 1){

            System.err.println("\033[1;31m remplace() caseJ pas dans les bornes\033[1;37m");

        } else {

            if (jActuel == j1){

                jeu[(caseJ-1)] = "\033[1;31mo";

            } else {

                jeu[(caseJ-1)] = "\033[1;32mx";
            }
        }
     }

     /**
      * Permet au Bot de jouer un coup aléatoirement
      * @param jeu le plateau du morpion
      * @return le numéro de la case dans laquelle le bot va jouer
      */
    public static int botJoue (String[] jeu){
        int ret = -1;
        int coup = 0;

        coup = botPeutGagner(jeu);
        if (coup == 0){
            coup = coupPerdant(jeu);
        }

        if (coup == 0){
            ret = (int) (Math.random() * 9);

            while (estPossible(jeu, ret) == false){
                ret = (int)(Math.random() * 11) + 0;
            }

        } else {
            if ((estPossible(jeu, coup) == true) && (estInclus(coup, coups) == false)){
                ret = coup;

            } else {
                ret = (int) (Math.random() * 9);
                while (estPossible(jeu, ret) == false){
                    ret = (int)(Math.random() * 11) + 0;
                }
            }
        }

        return ret;
     }

     /**
      * Permet de savoir si un int est inclus dans un Arraylist<Integer>
      * @param caseJ le chiffre à rechercher dans l'arraylist
      * @param coups l'arraylist des coups déjà joué
      * @return true si caseJ est dans coups
      */
    public static boolean estInclus (int caseJ, ArrayList<Integer> coups){
        boolean ret = false;
        int i = 0;
        if (caseJ > 0 || caseJ < 10){
            if (tour > 0){
                while (i < coups.size()){
                    if (caseJ == coups.get(i)){
                        ret = true;
                    }
                    i++;
                }
            }
        } else {
            ret = true;
        }

        return ret;
     }
/**
 * Permet de changer de joueur à chaque tour
 * @param jActuel le nom du joueur actuel
 * @param j1 le joueur 
 * @param j2 le joueur 
 * @return le nom du joueur qui jouera au prochain tour
 */
     public static String changeJoueur (String jActuel, String j1, String j2){
        String ret;

        if (jActuel == j2){
            ret = j1;

        } else {

            ret = j2;
        }

        return ret;
     }

     public static void aJoue (String jActuel, String j1, String j2, int caseJ){
        System.out.println();
        if (jActuel == j2){
            System.out.println();
            System.out.println("\033[1;32m" + j2 + " \033[1;37ma joué dans la case n°\033[1;32m" + caseJ + "\033[1;37m");

        } else {

            System.out.println("\033[1;31m" + j1 + " \033[1;37ma joué dans la case n°\033[1;31m" + caseJ + "\033[1;37m");
        }
        System.out.println();
     }

    public static boolean jeuGagnant (String[] jeu){
        boolean ret = false;

        if (jeu[0] == jeu[1] && jeu[0] == jeu[2]){
            ret = true;
        }
        if ((jeu[3] == jeu[4] && jeu[3] == jeu[5]) && ret == false){
            ret = true;
        }
        if ((jeu[6] == jeu[7] && jeu[6] == jeu[8]) && ret == false){
            ret = true;
        }


        if ((jeu[0] == jeu[3] && jeu[0] == jeu[6]) && ret == false){
            ret = true;
        }
        if ((jeu[1] == jeu[4] && jeu[1] == jeu[7]) && ret == false){
            ret = true;
        }
        if ((jeu[2] == jeu[5] && jeu[2] == jeu[8]) && ret == false){
            ret = true;
        }


        if ((jeu[0] == jeu[4] && jeu[0] == jeu[8]) && ret == false){
            ret = true;
        }
        if ((jeu[2] == jeu[4] && jeu[2] == jeu[6]) && ret == false){
            ret = true;
        }

        if (ret == true){
            gagne = 1;
        }
        return ret;
     }

     /**
      * Permet de déterminer si le bot peut empecher l'adversaire de gagner
      * @param jeu le plateau de jeu    
      * @return le numéro de la case pour jouer le bon coup
      */
    public static int coupPerdant (String[] jeu){
        int ret = 0;

        //Horizontales
        if ((jeu[0] == jeu[1]) && ((jeu[0] == "\033[1;32mx") && (jeu[1] == "\033[1;32mx"))){
            ret = 3;
        }
        if ((jeu[1] == jeu[2]) && ((jeu[1] == "\033[1;32mx") && (jeu[2] == "\033[1;32mx"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[2]) && ((jeu[0] == "\033[1;32mx") && (jeu[2] == "\033[1;32mx"))){
            ret = 2;
        }

        if ((jeu[3] == jeu[4]) && ((jeu[3] == "\033[1;32mx") && (jeu[4] == "\033[1;32mx"))){
            ret = 6;
        }
        if ((jeu[4] == jeu[5]) && ((jeu[4] == "\033[1;32mx") && (jeu[5] == "\033[1;32mx"))){
            ret = 4;
        }
        if ((jeu[3] == jeu[5]) && ((jeu[3] == "\033[1;32mx") && (jeu[5] == "\033[1;32mx"))){
            ret = 5;
        }       
        
        if ((jeu[6] == jeu[7]) && ((jeu[6] == "\033[1;32mx") && (jeu[7] == "\033[1;32mx"))){
            ret = 9;
        }
        if ((jeu[7] == jeu[8]) && ((jeu[7] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 7;
        }
        if ((jeu[6] == jeu[8]) && ((jeu[6] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 8;
        }

        //Verticales
        if ((jeu[0] == jeu[3]) && ((jeu[0] == "\033[1;32mx") && (jeu[3] == "\033[1;32mx"))){
            ret = 7;
        }
        if ((jeu[3] == jeu[6]) && ((jeu[3] == "\033[1;32mx") && (jeu[6] == "\033[1;32mx"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[6]) && ((jeu[0] == "\033[1;32mx") && (jeu[6] == "\033[1;32mx"))){
            ret = 4;
        }

        if ((jeu[1] == jeu[4]) && ((jeu[1] == "\033[1;32mx") && (jeu[4] == "\033[1;32mx"))){
            ret = 8;
        }
        if ((jeu[4] == jeu[7]) && ((jeu[4] == "\033[1;32mx") && (jeu[7] == "\033[1;32mx"))){
            ret = 2;
        }
        if ((jeu[1] == jeu[7]) && ((jeu[1] == "\033[1;32mx") && (jeu[7] == "\033[1;32mx"))){
            ret = 5;
        }

        if ((jeu[2] == jeu[5]) && ((jeu[2] == "\033[1;32mx") && (jeu[5] == "\033[1;32mx"))){
            ret = 9;
        }
        if ((jeu[5] == jeu[8]) && ((jeu[5] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 3;
        }
        if ((jeu[2] == jeu[8]) && ((jeu[2] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 6;
        }

        //Diagonales
        if ((jeu[0] == jeu[4]) && ((jeu[0] == "\033[1;32mx") && (jeu[4] == "\033[1;32mx"))){
            ret = 9;
        }
        if ((jeu[4] == jeu[8]) && ((jeu[4] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[8]) && ((jeu[0] == "\033[1;32mx") && (jeu[8] == "\033[1;32mx"))){
            ret = 5;
        }

        if ((jeu[2] == jeu[4]) && ((jeu[2] == "\033[1;32mx") && (jeu[4] == "\033[1;32mx"))){
            ret = 7;
        }
        if ((jeu[4] == jeu[6]) && ((jeu[4] == "\033[1;32mx") && (jeu[6] == "\033[1;32mx"))){
            ret = 3;
        }
        if ((jeu[2] == jeu[6]) && ((jeu[2] == "\033[1;32mx") && (jeu[6] == "\033[1;32mx"))){
            ret = 5;
        }     

        return ret;
     }

    public static int botPeutGagner (String[] jeu){
        int ret = 0;

        //Horizontales
        if ((jeu[0] == jeu[1]) && ((jeu[0] == "\033[1;31mo") && (jeu[1] == "\033[1;31mo"))){
            ret = 3;
        }
        if ((jeu[1] == jeu[2]) && ((jeu[1] == "\033[1;31mo") && (jeu[2] == "\033[1;31mo"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[2]) && ((jeu[0] == "\033[1;31mo") && (jeu[2] == "\033[1;31mo"))){
            ret = 2;
        }

        if ((jeu[3] == jeu[4]) && ((jeu[3] == "\033[1;31mo") && (jeu[4] == "\033[1;31mo"))){
            ret = 6;
        }
        if ((jeu[4] == jeu[5]) && ((jeu[4] == "\033[1;31mo") && (jeu[5] == "\033[1;31mo"))){
            ret = 4;
        }
        if ((jeu[3] == jeu[5]) && ((jeu[3] == "\033[1;31mo") && (jeu[5] == "\033[1;31mo"))){
            ret = 5;
        }       
        
        if ((jeu[6] == jeu[7]) && ((jeu[6] == "\033[1;31mo") && (jeu[7] == "\033[1;31mo"))){
            ret = 9;
        }
        if ((jeu[7] == jeu[8]) && ((jeu[7] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 7;
        }
        if ((jeu[6] == jeu[8]) && ((jeu[6] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 8;
        }

        //Verticales
        if ((jeu[0] == jeu[3]) && ((jeu[0] == "\033[1;31mo") && (jeu[3] == "\033[1;31mo"))){
            ret = 7;
        }
        if ((jeu[3] == jeu[6]) && ((jeu[3] == "\033[1;31mo") && (jeu[6] == "\033[1;31mo"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[6]) && ((jeu[0] == "\033[1;31mo") && (jeu[6] == "\033[1;31mo"))){
            ret = 4;
        }

        if ((jeu[1] == jeu[4]) && ((jeu[1] == "\033[1;31mo") && (jeu[4] == "\033[1;31mo"))){
            ret = 8;
        }
        if ((jeu[4] == jeu[7]) && ((jeu[4] == "\033[1;31mo") && (jeu[7] == "\033[1;31mo"))){
            ret = 2;
        }
        if ((jeu[1] == jeu[7]) && ((jeu[1] == "\033[1;31mo") && (jeu[7] == "\033[1;31mo"))){
            ret = 5;
        }

        if ((jeu[2] == jeu[5]) && ((jeu[2] == "\033[1;31mo") && (jeu[5] == "\033[1;31mo"))){
            ret = 9;
        }
        if ((jeu[5] == jeu[8]) && ((jeu[5] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 3;
        }
        if ((jeu[2] == jeu[8]) && ((jeu[2] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 6;
        }

        //Diagonales
        if ((jeu[0] == jeu[4]) && ((jeu[0] == "\033[1;31mo") && (jeu[4] == "\033[1;31mo"))){
            ret = 9;
        }
        if ((jeu[4] == jeu[8]) && ((jeu[4] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 1;
        }
        if ((jeu[0] == jeu[8]) && ((jeu[0] == "\033[1;31mo") && (jeu[8] == "\033[1;31mo"))){
            ret = 5;
        }

        if ((jeu[2] == jeu[4]) && ((jeu[2] == "\033[1;31mo") && (jeu[4] == "\033[1;31mo"))){
            ret = 7;
        }
        if ((jeu[4] == jeu[6]) && ((jeu[4] == "\033[1;31mo") && (jeu[6] == "\033[1;31mo"))){
            ret = 3;
        }
        if ((jeu[2] == jeu[6]) && ((jeu[2] == "\033[1;31mo") && (jeu[6] == "\033[1;31mo"))){
            ret = 5;
        }

        return ret;
     }
     /*
      * Affiche les println de début de jeu + crédits...
      */
    public static void intro (){
        System.out.println();
        System.out.println("\033[0m\033[0;32mLancement de Morpion");
        attendre(500);
        System.out.println("Version actuelle : \033[0;35mV" + version + " \033[0;32mVersion Bot : \033[0;35m" + vBot + "\033[0;32m");
        attendre(500);
        System.out.println("Developpé par MEMAIN Maël");
        attendre(500);
        System.out.print("Début : 23/01/2023 ");
        attendre(200);
        System.out.println("Fin : 24/01/2023");
        attendre(500);
        System.out.println("Etat : " + etat);
        System.out.println();
     }

    public static void attendre (long temps){
        long t1 = System.currentTimeMillis();
        long t2 = 0;
        long diffT = 0;
        diffT = t2 - t1;
        while (diffT < temps){
            t2 = System.currentTimeMillis();
            diffT = t2 - t1;
        }
     }

     /**
      * Permet de recommencer une partie après la fin d'une partie si le joueur le décide
      */
    public static void rejouer (){
        int rej = -1;
        attendre(500);
        System.out.println();
        System.out.println("\033[0;1mVoulez-vous rejouer ?");
        attendre(200);
        System.out.println();
        System.out.println("\033[0;1m- 0 : Rejouer");
        System.out.println("\033[0;1m- 1 : Quitter");
        while ((rej < 0) || (rej > 1)){
            rej = SimpleInput.getInt("- ");
        }
        if (rej == 1){
            System.exit(0);
        } else {
            jouer(jeu);
            attendre(200);
            System.out.println("\033[0;1mUne partie va recommencer");
        }
     }
}
